﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveManager : MonoBehaviour {

    public Camera m_camera;

    public float m_cameraSpeed;
    public float m_smoothing;

    public float m_zoomStep;
    public float m_zoomMax;
    public float m_zoomMin;

    private int m_mapSize;
    private Vector3 m_targetCamPos;
    private float m_targetCamZoom;

    // Use this for initialization
    void Start () {
        CellManager cellManager = GetComponent<CellManager>();
        m_mapSize = cellManager.m_mapSize;
        
        // Move camera to center
        m_camera.transform.position = new Vector3(m_mapSize / 2.0f + 0.5f, m_mapSize / 2.0f + 0.5f, -10.0f);
        m_targetCamPos = m_camera.transform.position;
        m_targetCamZoom = m_camera.orthographicSize;
    }
	
	// Update is called once per frame
	void Update () {
        float vertical = Input.GetAxisRaw("Vertical");
        float horizontal = Input.GetAxisRaw("Horizontal");
        if (vertical != 0 || horizontal != 0) {
            Vector3 move = m_camera.transform.right * horizontal + m_camera.transform.up * vertical;
            move.Normalize();
            m_targetCamPos += move * m_cameraSpeed * Time.deltaTime;
            m_targetCamPos.x = Mathf.Clamp(m_targetCamPos.x, 0, m_mapSize);
            m_targetCamPos.y = Mathf.Clamp(m_targetCamPos.y, 0, m_mapSize);
        }
        
        float mouseWheel = Input.GetAxisRaw("Mouse ScrollWheel");
        if (mouseWheel != 0) {
            m_targetCamZoom = Mathf.Clamp(m_targetCamZoom + m_zoomStep * -mouseWheel, m_zoomMin, m_zoomMax);
        }

        m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, m_targetCamPos, m_smoothing * Time.deltaTime);
        m_camera.orthographicSize = Mathf.Lerp(m_camera.orthographicSize, m_targetCamZoom, m_smoothing * Time.deltaTime);
    }
}
