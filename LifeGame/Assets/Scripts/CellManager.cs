﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class CellManager : MonoBehaviour {

    public GameObject m_cellContainer;
    public GameObject m_cellObject;
    public GameObject m_addCellObject;
    public GameObject m_removeCellObject;

    public Text m_statusText;

    public Camera m_camera;

    public int m_mapSize = 100;
    public float[] m_simulationSpeeds;

    private GameObject m_addCellCursor;
    private GameObject m_removeCellCursor;
    private GameObject[] m_cellMap;
    private BoxCollider2D m_boxCollider;

    private bool m_simulationRunning = false;
    private float m_lastTimeLoop, m_nextTimeLoop, m_loopSpeed;
    private int m_loopCount = 0;
    private int m_currentSpeedIndex;
    private bool m_showCursor = false;
    private Vector2 m_cursorPosition;

    private Queue m_checkOrders;
    private Queue m_removeOrders;
    private Queue m_addOrders;
    private Queue m_cellReserve;

    // Use this for initialization
    void Start() {
        m_boxCollider = GetComponent<BoxCollider2D>();
        m_cellMap = new GameObject[m_mapSize * m_mapSize];
        m_boxCollider.size = new Vector2(m_mapSize, m_mapSize);
        m_boxCollider.offset = new Vector2(m_mapSize / 2, m_mapSize / 2);

        // Create cell cursors
        m_addCellCursor = Instantiate<GameObject>(m_addCellObject, m_cellContainer.transform);
        m_addCellCursor.SetActive(false);
        m_removeCellCursor = Instantiate<GameObject>(m_removeCellObject, m_cellContainer.transform);
        m_removeCellCursor.SetActive(false);

        // Create queues
        m_checkOrders = new Queue();
        m_removeOrders = new Queue();
        m_addOrders = new Queue();
        m_cellReserve = new Queue();

        // Update UI
        UpdateGUI();

        // Time loop
        m_lastTimeLoop = -1.0f;
        m_nextTimeLoop = -1.0f;
        m_currentSpeedIndex = m_simulationSpeeds.Length - 1;
        m_loopSpeed = m_simulationSpeeds[m_simulationSpeeds.Length - 1];

        Debug.Log("Startup loop speed = " + m_loopSpeed);
    }

    void UpdateGUI() {
        m_statusText.text = (m_simulationRunning ? "Running" : "Paused") + " - " + m_loopCount + "\n" +
            "Speed " + m_loopSpeed.ToString() + "s";
    }

    bool CheckRules(int x, int y, bool isAlive) {
        int neighboors = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i >= 0 && i < m_mapSize && j >= 0 && j < m_mapSize && (i != x || j != y) && (j != y || i != x) && m_cellMap[j * m_mapSize + i] != null) {
                    neighboors += 1;
                }
            }
        }
        return neighboors == 3 || (isAlive && neighboors == 2);
    }

    void UpdateCheckOrder(int x, int y) {
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i >= 0 && i < m_mapSize && j >= 0 && j < m_mapSize && (i != x || j != y) && (j != y || i != x) && m_cellMap[j * m_mapSize + i] == null && !m_checkOrders.Contains(new Vector2(i, j))) {
                    m_checkOrders.Enqueue(new Vector2(i, j));
                }
            }
        }
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetButtonUp("Jump")) {
            m_simulationRunning = !m_simulationRunning;

            if (m_lastTimeLoop == -1.0f) {
                m_lastTimeLoop = Time.time;
                m_nextTimeLoop = m_lastTimeLoop + m_loopSpeed;
                Debug.Log("First start loop speed = " + m_loopSpeed);
            }
        }
        if (Input.GetButtonUp("Fire1")) {
            m_currentSpeedIndex = Mathf.Max(m_currentSpeedIndex - 1, 0);
            m_loopSpeed = m_simulationSpeeds[m_currentSpeedIndex];
            m_nextTimeLoop = m_lastTimeLoop + m_loopSpeed;
        }
        if (Input.GetButtonUp("Fire3")) {
            m_currentSpeedIndex = Mathf.Min(m_currentSpeedIndex + 1, m_simulationSpeeds.Length - 1);
            m_loopSpeed = m_simulationSpeeds[m_currentSpeedIndex];
            m_nextTimeLoop = m_lastTimeLoop + m_loopSpeed;
        }
        if (Input.GetButtonUp("Cancel")) {
            m_simulationRunning = false;
            m_loopCount = 0;
            m_cellReserve.Clear();
            GameObject[] activeCells = GameObject.FindGameObjectsWithTag("Cell");
            foreach (GameObject cell in activeCells) {
                if (cell.activeSelf) {
                    int coordX = Mathf.FloorToInt(cell.transform.position.x);
                    int coordY = Mathf.FloorToInt(cell.transform.position.y);
                    m_cellMap[coordX * m_mapSize + coordY] = null;
                }
                Destroy(cell);
            }
        }

        if (m_simulationRunning) {
            if (Time.time > m_nextTimeLoop) {

                Debug.Log("Execute loop " + m_loopCount + "-" + m_loopSpeed);
                
                GameObject[] activeCells = GameObject.FindGameObjectsWithTag("Cell");
                foreach (GameObject cell in activeCells) {
                    if (cell.activeSelf) {
                        int coordX = Mathf.FloorToInt(cell.transform.position.x);
                        int coordY = Mathf.FloorToInt(cell.transform.position.y);
                        // Check rules and add a delete order if needed
                        bool survive = CheckRules(coordX, coordY, true);
                        if (!survive) {
                            m_removeOrders.Enqueue(new Vector2(coordX, coordY));
                        }
                        // Add check order for neighboors
                        UpdateCheckOrder(coordX, coordY);
                    }
                }
                // For each check order
                // Check rules and add a add order if needed
                while (m_checkOrders.Count != 0) {
                    Vector2 checkPos = (Vector2)m_checkOrders.Dequeue();
                    bool born = CheckRules((int)checkPos.x, (int)checkPos.y, false);
                    if (born) {
                        m_addOrders.Enqueue(checkPos);
                    }
                }

                // Execute delete orders
                while (m_removeOrders.Count != 0) {
                    Vector2 removePos = (Vector2)m_removeOrders.Dequeue();
                    GameObject removeCell = m_cellMap[(int)removePos.y * m_mapSize + (int)removePos.x];
                    m_cellMap[(int)removePos.y * m_mapSize + (int)removePos.x] = null;
                    removeCell.SetActive(false);
                    m_cellReserve.Enqueue(removeCell);
                }

                // Execute add orders
                while(m_addOrders.Count != 0) {
                    Vector2 addPos = (Vector2)m_addOrders.Dequeue();
                    GameObject addCell;
                    if (m_cellReserve.Count > 0)
                        addCell = (GameObject)m_cellReserve.Dequeue();
                    else
                        addCell = Instantiate<GameObject>(m_cellObject, m_cellContainer.transform);
                    addCell.transform.position = new Vector2(addPos.x + 0.5f, addPos.y + 0.5f);
                    addCell.SetActive(true);
                    m_cellMap[(int)addPos.y * m_mapSize + (int)addPos.x] = addCell;
                }

                // Clean buffers

                m_loopCount++;
                m_lastTimeLoop = Time.time;
                m_nextTimeLoop = m_lastTimeLoop + m_loopSpeed;
            }
        }

        if (m_showCursor && !m_simulationRunning) {
            m_cursorPosition = m_camera.ScreenToWorldPoint(Input.mousePosition);
            int coordX = Mathf.FloorToInt(m_cursorPosition.x);
            int coordY = Mathf.FloorToInt(m_cursorPosition.y);
            GameObject activeCursor, unactiveCursor;
            if (m_cellMap[coordY * m_mapSize + coordX] == null) {
                activeCursor = m_addCellCursor;
                unactiveCursor = m_removeCellCursor;
            } else {
                activeCursor = m_removeCellCursor;
                unactiveCursor = m_addCellCursor;
            }
            Vector2 discretPos = new Vector2(Mathf.Floor(m_cursorPosition.x) + 0.5f, Mathf.Floor(m_cursorPosition.y) + 0.5f);
            activeCursor.transform.position = discretPos;
            activeCursor.SetActive(true);
            unactiveCursor.SetActive(false);
        } else {
            m_addCellCursor.SetActive(false);
            m_removeCellCursor.SetActive(false);
        }

        UpdateGUI();
    }

    void OnMouseEnter() {
        m_showCursor = true;
    }
    void OnMouseExit() {
        m_showCursor = false;
    }
    void OnMouseUpAsButton() {
        Debug.Log("OnMouseUpAsButton");
        Vector3 mousePos = m_camera.ScreenToWorldPoint(Input.mousePosition);
        int coordX = Mathf.FloorToInt(mousePos.x);
        int coordY = Mathf.FloorToInt(mousePos.y);
        Debug.Log("Click on coord " + coordX + "-" + coordY);

        if (m_cellMap[coordY * m_mapSize + coordX] == null) {
            GameObject newCell = Instantiate<GameObject>(m_cellObject, m_cellContainer.transform);
            newCell.transform.position = new Vector3(coordX + 0.5f, coordY + 0.5f, 0.0f);
            m_cellMap[coordY * m_mapSize + coordX] = newCell;
        } else {
            GameObject cellToRemove = m_cellMap[coordY * m_mapSize + coordX];
            m_cellMap[coordY * m_mapSize + coordX] = null;
            Destroy(cellToRemove);
        }
    }
}
